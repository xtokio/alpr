# Automatic License Plate Recognition ( ALPR )

## Servidor Ubuntu 18.04

#### Actualizar servidor

    sudo apt update

#### Instalar servidor web apache

    sudo apt install apache2
    sudo ufw app info "Apache Full"
    sudo ufw allow in "Apache Full"

#### Instalar PHP

    sudo apt install php libapache2-mod-php php-mysql

#### Instalar OpenALPR

    sudo apt install openalpr
    cp /usr/share/openalpr/runtime_data/ocr/tessdata/*.traineddata /usr/share/openalpr/runtime_data/ocr/

#### Instalar GIT ( para descargar el proyecto web )

    sudo apt install git

#### Instalar proyecto web

    cd /var/www/html/
    git clone git@bitbucket.org:xtokio/alpr.git
    sudo chown www-data:www-data -R alpr

