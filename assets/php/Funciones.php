<?php
//Obtener info de placas de Baja California
//curl -X POST 'http://www3.ebajacalifornia.gob.mx/Placas/presupuestoPlaca.jsp?PLACA=AP27043&TRAMITE=C&MENSAJERIA=N&tipoPersona=N&usoVehiculo=N'
//Obtener info de placas de EUA
//curl -X POST 'https://www.carfax.com/processQuickVin.cfx?licensePlateNumber=XGA623&licensePlateState=IN'

ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_FILES['upload_image'])) 
{
    $Placa = "Plate not detected";
    $target_dir = "../img/uploads/";
    $filename = uniqid_base36()."_".str_replace(' ', '', $_FILES["upload_image"]["name"]);
    $target_file = $target_dir . basename($filename);
    $uploadOk = 0;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    if(move_uploaded_file($_FILES["upload_image"]["tmp_name"], $target_file))
    {
        //Crop Image
        //$Imagen = run("./crop_image $target_file");
        $Placa = detect($target_file);
        if (file_exists($target_file))
            unlink($target_file);
    }
    echo $Placa;    
}

function detect($Image)
{
    $Placa = "Plate not detected";
    $Country = "";

    //Placa US
    $Placa_US = run("alpr -c us -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_US = array_filter($Placa_US);
    //$Placa = $Placa_US[0];
    if(empty($Placa_US[0]))
        $Placa_US[0] = "";
    
    //Placa EU
    $Placa_EU = run("alpr -c eu -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_EU = array_filter($Placa_EU);

    if(empty($Placa_EU[0]))
        $Placa_EU[0] = "";
    
    //Placa AU
    $Placa_AU = run("alpr -c au -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_AU = array_filter($Placa_AU);

    if(empty($Placa_AU[0]))
        $Placa_AU[0] = "";
   
    if(strlen($Placa_US[0]) >= strlen($Placa_EU[0]) && strlen($Placa_US[0]) >= strlen($Placa_AU[0]))
    {
        $Placa = $Placa_US[0];
        $Country = " - US";
    }
    if(strlen($Placa_EU[0]) > strlen($Placa_US[0]) && strlen($Placa_EU[0]) > strlen($Placa_AU[0]))
    {
        $Placa = $Placa_EU[0];
        $Country = " - MX";
    }
    if(strlen($Placa_AU[0]) > strlen($Placa_EU[0]) && strlen($Placa_AU[0]) > strlen($Placa_US[0]))
    {
        $Placa = $Placa_AU[0];
        $Country = " - MX";
    }
            
    if(strlen($Placa_US[0]) == strlen($Placa_EU[0]) && strlen($Placa_US[0]) == strlen($Placa_AU[0]))
    {
        if($Placa_US != "")
        {
            $Placa = $Placa_US[0];
            $Country = " - US 01";
        }
            
    }

    return $Placa.$Country;
}

function run($command)
{
	$output = array();
	exec($command,$output);
	return $output;
}

function uniqid_base36($more_entropy=false) 
{
    $s = uniqid('', $more_entropy);
    if (!$more_entropy)
        return base_convert($s, 16, 36);
        
    $hex = substr($s, 0, 13);
    $dec = $s[13] . substr($s, 15); // skip the dot
    return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
}

?>